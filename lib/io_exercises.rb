# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.


def guessing_game

  secret_number = rand(1..100)
  count = 0

  puts "Guess a number between 1 and 100"


  while true

    user_guess = gets.chomp.to_i

    puts "You guessed #{user_guess}"

    if user_guess == secret_number
      count += 1
      break
    elsif user_guess < 1
      puts "Sorry that is too low, Please try again"
    elsif user_guess < 1 || user_guess > 100
        puts 'Choose a number between 1 and 100'
    elsif user_guess > secret_number
      puts 'too high'
      count += 1
    elsif user_guess < secret_number
      puts 'too low'
      count += 1

    end
  end

  puts "Congrats! After #{count} guesses, #{user_guess} is correct!"

end
